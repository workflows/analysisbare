# analysis base version from which we start
FROM rootproject/root-conda:6.18.04

# Setup user and permissions
RUN adduser johndoe -u 501 --disabled-password --gecos ""
RUN usermod -a -G 0 johndoe
USER johndoe

# put the code into the repo with the proper owner
COPY --chown=johndoe . /code/src

# note that the build directory already exists in /code/src
RUN cd /code/src && \
    g++ `root-config --glibs --cflags` -o analysis.exe analysis.cpp

# where you are left after booting the image
WORKDIR /code/src


